package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/** セッションにログイン情報がある場合、ユーザー一覧のサーブレットにリダイレクト **/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") != null) {
			response.sendRedirect("/UserManagement/UserListServlet");
		} else {
		// login.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/login.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/** リクエストパラメータの文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** 入力されたログインIDとパスワードを取得 **/
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		/** パスワードを暗号化 **/
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		password = DatatypeConverter.printHexBinary(bytes);

		/** 取得したログインIDとパスワードを引数に渡し、daoのメソッドを実行し検索 **/
		UserDao userdao = new UserDao();
		User user = userdao.findByLoginInfo(loginId, password);

		/** テーブルに該当のデータが見つからなかった場合(ログイン失敗時) **/
		if (user == null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
			// 再びlogin.jspへフォワードし、エラーメッセージを表示
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** テーブルに該当のデータが見つかった場合(ログイン成功時)
		    セッションスコープにユーザーの情報をセット **/
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		/** ユーザー一覧のサーブレットにリダイレクト **/
		response.sendRedirect("/UserManagement/UserListServlet");

	}

}

package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** リクエストパラメータの文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** IDを取得**/
		String id = request.getParameter("id");

		/** Daoでユーザーを検索**/
		UserDao userdao = new UserDao();
		User user = userdao.findUser(id);

		/** リクエストスコープにユーザー情報をセット**/
		request.setAttribute("user", user);

		/** dalete.jspにフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** リクエストパラメータの文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** IDを取得**/
		String id = request.getParameter("id");

		/** daoでユーザーを削除**/
		UserDao userdao = new UserDao();
		int result = userdao.userDelete(id);

		/** 削除に成功していたらユーザー一覧画面に戻る
		    失敗した場合削除画面に戻りエラーメッセージを表示 **/
		if (result == 1) {
			response.sendRedirect("/UserManagement/UserListServlet");
		} else {
			request.setAttribute("errMsg", "削除を失敗しました");
			request.setAttribute("id", id);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/delete.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}

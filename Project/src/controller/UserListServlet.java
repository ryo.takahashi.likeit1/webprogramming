package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}


		/** ユーザー情報テーブルに登録されている管理者以外のすべてのユーザーを取得 **/
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		/** リクエストスコープにユーザー一覧情報をセット **/
		request.setAttribute("userList", userList);

		/** list.jspにフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** リクエストパラメータの文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** 入力された値を取得 **/
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");

		/** daoで検索**/
		UserDao userdao = new UserDao();
		List<User> userList = userdao.searchUser(loginId, name, fromDate, toDate);

		/** 検索結果がある場合、リクエストスコープにユーザー情報をしlist.jspにフォワード
	    	検索結果がない場合、エラーメッセージをセットしlist.jspにフォワード**/
		if (!userList.isEmpty()) {
			request.setAttribute("userList", userList);
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("fromDate", fromDate);
			request.setAttribute("toDate", toDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/list.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
			request.setAttribute("errMsg", "該当するユーザーが見つかりませんでした");
			userList = userdao.findAll();
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("fromDate", fromDate);
			request.setAttribute("toDate", toDate);
			request.setAttribute("userList", userList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lib/jsp/list.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}

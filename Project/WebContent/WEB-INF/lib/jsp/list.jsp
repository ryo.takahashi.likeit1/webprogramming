<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">

<title>ユーザー一覧</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<link rel="stylesheet" href="css/style.css">

</head>

<body>

	<div class="row"
		style="margin-left: 0; margin-right: 0; background-color: cadetblue; height: 35px">
		<div class="col align-self-center" style="text-align: right">
			<!-- ログイン中のユーザー名の表示 -->
			<a style="color: aliceblue"> ${userInfo.name } </a> <a class="pr-5"
				style="color: aliceblue">さん</a> <a
				href="/UserManagement/LogoutServlet" style="color: red"> <span
				style="text-decoration: underline">ログアウト</span>
			</a>
		</div>
	</div>


	<h1 class="title">ユーザー一覧</h1>

	<c:if test="${errMsg != null}">
		<p style="color: red; text-align: center">${errMsg }</p>
	</c:if>

	<div class="container">
		<div class="col-10 offset-1" style="text-align: right">
			<!-- ユーザー登録画面へ遷移 -->
			<p>
				<c:if test="${userInfo.loginId == 'admin' }">
					<a href="/UserManagement/UserRegistServlet" class="border-bottom"
						style="border-bottom-color: b"> 新規登録 </a>
				</c:if>
			</p>
		</div>

		<form action="/UserManagement/UserListServlet" method="post">
			<div class="row">
				<div class="col-3 offset-md-1">
					<p>ログインID</p>
				</div>
				<div class="col">
					<input name="loginId" value="${loginId}" type="text" style="width: 350px">
				</div>
			</div>

			<div class="row">
				<div class="col-3 offset-md-1">
					<p>ユーザー名</p>
				</div>
				<div class="col">
					<input name="name" value="${name}" type="text" style="width: 350px">
				</div>
			</div>

			<div class="row">
				<div class="col-3 offset-md-1">
					<p>生年月日</p>
				</div>
				<div class="col">
					<p>
						<input name="fromDate" value="${fromDate}" type="date" style="width: 155px"><span
							class="space">～</span><input name="toDate" value="${toDate}" type="date" style="width: 155px">
					</p>
				</div>
			</div>

			<div class="search col-10 offset-1" style="text-align: right">
				<input type="submit" value="検索" style="width: 120px">
			</div>
		</form>

	</div>


	<div class="mt-4">
		<table class="container">
			<tr class="row">
				<th class="result col-2 offset-1">ログインID</th>
				<th class="result col-2">ユーザー名</th>
				<th class="result col-3">生年月日</th>
				<th class="result col-3"></th>
			</tr>
			<!-- 登録されているユーザーの数毎に列を追加 -->
			<c:forEach var="user" items="${userList}">
				<tr class="row" style="text-align: center">
					<td class="cel col-2 offset-1">${user.loginId}</td>
					<td class="cel col-2">${user.name}</td>
					<td class="cel col-3">${user.birthDate}</td>
					<td class="cel col-3">
						<div class="row" style="text-align: center">
							<div class="col">
								<a class="choice btn btn-sm btn-info"
									href="/UserManagement/UserInfoServlet?id=${user.id}"> 詳細 </a>
							</div>
							<c:if
								test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
								<div class="col">
									<a class="choice btn btn-sm btn-success"
										href="/UserManagement/UserEditServlet?id=${user.id}"> 更新 </a>
								</div>
							</c:if>
							<c:if test="${userInfo.loginId == 'admin' }">
								<div class="col">
									<a class="choice btn btn-sm btn-danger"
										href="/UserManagement/UserDeleteServlet?id=${user.id}"> 削
										除 </a>
								</div>
							</c:if>
						</div>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>
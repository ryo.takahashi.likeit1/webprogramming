<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>ユーザー削除確認</title>

   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="row" style="margin-left: 0; margin-right: 0;background-color: cadetblue;height:35px">
        <div class="col align-self-center" style="text-align: right">
            <!-- ログイン中のユーザー名の表示 -->
            <a style="color: aliceblue">
                ${userInfo.name }
            </a>
            <a class="pr-5" style="color: aliceblue">さん</a>
            <a href="/UserManagement/LogoutServlet" style="color: red">
                <span style="text-decoration: underline">ログアウト</span>
            </a>
        </div>
    </div>

    <h1 class="title">ユーザー削除確認</h1>

	<c:if test="${errMsg != null}">
		<p style="color: red; text-align: center">${errMsg }</p>
	</c:if>

    <div class="col-4 mx-auto">
        <div class="offset-sm-2">
            ログインID : ${user.loginId}${id}<br>を本当に削除してよろしいてしょうか。
        </div>

        <div class="row mt-5" style="text-align: center">
            <div class="col-6">
            	<form action="/UserManagement/UserListServlet">
                	<input type="submit" value="キャンセル" style="width: 100px">
                </form>
            </div>
            <div class="col-6">
                <form action="/UserManagement/UserDeleteServlet" method="post">
                    <!-- idを引き継がせる  -->
                    <input type="hidden" name="id" value="${user.id}">
                    <input type="submit" value="OK" style="width: 100px">
                </form>
            </div>
        </div>
    </div>

</body>
</html>
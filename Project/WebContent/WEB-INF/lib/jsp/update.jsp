<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>ユーザー情報更新</title>

   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="row" style="margin-left: 0; margin-right: 0;background-color: cadetblue;height:35px">
        <div class="col align-self-center" style="text-align: right">
            <!-- ログイン中のユーザー名の表示 -->
            <a style="color: aliceblue">
                ${userInfo.name }
            </a>
            <a class="pr-5" style="color: aliceblue">さん</a>
            <a href="/UserManagement/LogoutServlet" style="color: red">
                <span style="text-decoration: underline">ログアウト</span>
            </a>
        </div>
    </div>

    <h1 class="title">ユーザー情報更新</h1>

    <c:if test="${errMsg != null}">
		<p style="color: red; text-align: center">${errMsg }</p>
	</c:if>

    <div class="col-6 mx-auto">
        <form action="/UserManagement/UserEditServlet" method="post">

        <!-- idとloginIdを引き継がせる  -->
        <input type="hidden" name="id" value="${user.id}${id}">
        <input type="hidden" name="loginId" value="${user.loginId}${loginId}">

        <table class="offset-sm-2">
            <tr>
                <td width=300 height=55>ログインID</td>
                <td width=300>${user.loginId}${loginId}</td>
            </tr>
            <tr>
                <td width=300 height=55>パスワード</td>
                <td width=300>
                    <input name="password" type="password" style="width: 200px">
                </td>
            </tr>
            <tr>
                <td width=300 height=55>パスワード(確認)</td>
                <td width=300>
                	<input name="confirm" type="password" style="width: 200px">
                </td>
            </tr>
            <tr>
                <td width=300 height=55>ユーザー名</td>
                <td width=300>
                	<input name="name" type="text" style="width: 200px" value="${user.name}${name}">
                </td>
            </tr>
            <tr>
                <td width=300 height=55>生年月日</td>
                <td width=300>
                	<input name="birthDate" type="date" style="width: 200px" value="${user.birthDate}${birthDate}">
                </td>
            </tr>
        </table>

        <div class="pt-4" style="text-align: center">
            <input type="submit" style="width: 100px">
        </div>
		</form>

        <div class="mt-5">
            <a href="/UserManagement/UserListServlet">
                <span style="text-decoration: underline">戻る</span>
            </a>
        </div>

    </div>
</body>
</html>
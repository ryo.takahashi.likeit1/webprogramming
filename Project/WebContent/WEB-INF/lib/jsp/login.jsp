<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>ログイン</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <h1 class="title">ログイン画面</h1>

	<c:if test="${errMsg != null}">
		<p style="color: red; text-align: center">${errMsg }</p>
	</c:if>


	<form action="/UserManagement/LoginServlet" method="post">
 	   <div class="container" style="text-align: center">
  	      <p>ログイン ID&emsp;<input type="text" name="loginId"></p>
   	 </div>

    	<div class="container" style="text-align: center">
    	    <p>パスワード &emsp;<input type="password" name="password"></p>
    	</div>

 	   <div class="container pt-5" style="text-align: center">
  	      <input type="submit" style="width: 100px" value="ログイン">
 	   </div>
    </form>

</body>

</html>